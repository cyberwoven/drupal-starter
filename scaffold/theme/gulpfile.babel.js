import gulp from "gulp";
import autoprefixer from "gulp-autoprefixer";
import sourcemaps from "gulp-sourcemaps";
import plumber from "gulp-plumber";
import notify from "gulp-notify";
import browserSync from "browser-sync";
import terser from "gulp-terser";
import rename from "gulp-rename";
import dartSass from 'sass';
import gulpSass from 'gulp-sass';
const sass = gulpSass(dartSass);

const siteUrl = '%DOMAIN%.test';

const browserSyncOptions = {
  proxy: siteUrl,
  host: siteUrl,
  port: 4000,
  open: false,
};

function styles(done) {
  gulp
    .src("sass/**/*.scss")
    .pipe(
      plumber({
        errorHandler: (err) => {
          notify.onError({
            title: "Gulp error in " + err.plugin,
            message: err.toString(),
          })(err);
          browserSync.notify("Error in CSS: " + err.message, 10000);
        },
      })
    )
    .pipe(sourcemaps.init())
    .pipe(sass({ outputStyle: "compressed", sourcemap: true })) // Compile to CSS
    .pipe(autoprefixer({ overrideBrowserslist: ["last 2 versions"] }))
    .pipe(sourcemaps.init())
    .pipe(sourcemaps.write("./"))
    .pipe(gulp.dest("css"))
    .pipe(browserSync.stream({ match: "**/*.css" }));
  browserSync.notify("Styles updating...");
  done();
}

function serve(done) {
  browserSync.init(browserSyncOptions);
  done();
}

function reload(done) {
  browserSync.notify("Browser refreshing...");
  browserSync.reload();
  done();
}

// Minify FE JS
function minify(done) {
  gulp
    .src([
      'js/src/*.js',
    ])
    .pipe(terser({ keep_fnames: true, mangle: false }))
    .pipe(sourcemaps.write("./"))
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest("js/min"))
    .pipe(browserSync.stream({ match: "**/*.js" }));
  browserSync.notify("JS updating...");
  done();
}

function watch() {
  gulp.watch("sass/**/*.scss", { usePolling: true, interval: 500 }, styles);
  gulp.watch(["js/src/*.js"], { usePolling: true, interval: 500 }, minify);
  gulp.watch(["js/min/*.js", "*.html", "*.php", "templates/**/*.twig", "*.theme"], { usePolling: true, interval: 500 }, reload);
  console.log("watch firing...");
}

const dev = () => gulp.series("styles", "serve", "minify", "watch")();

export { styles, serve, watch, minify };
export default dev;
