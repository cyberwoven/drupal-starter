# gulp-es6-boilerplate

- gulpfile.babel.js uses ES6 features
- SASS compilation with autoprefixer
- BrowserSync with hot reloading
- Can be used for Drupal theming (in sandbox) or locally (statically)
- ./sass folder was copied from the Cyberwoven Boilerplate
- gulp-sass-glob is installed. This means you can do things like `@import ../components/**.*.scss` in scss files.
- Each JS file in ./js/src is auto-minified and placed in ./js/min. We assume that JS files in ./js/vendor are already minified.
