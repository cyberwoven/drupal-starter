<?php

$db_name = '%DB_NAME%';

/**
 * Sandbox branch detection
 */
$raw_branch_name = exec('git rev-parse --abbrev-ref HEAD');
$branch_name = str_replace(['_', '.', '/'], '-', $raw_branch_name);
if (!in_array($branch_name, ['main', 'master', 'HEAD']) &&
    !empty($raw_branch_name) &&
    strtolower(substr($branch_name, 0, 6)) != 'hotfix') {
  $db_name .= '__' . $branch_name;
}

$databases['default']['default'] = [
  'database' => $db_name,
  'username' => $_SERVER['USER'],
  'password' => '',
  'prefix' => '',
  'host' => 'localhost',
  // 'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
];

/**
 * Where do private files live?
 */
$settings['file_private_path'] = realpath($app_root . '/../private_files');

$config['environment_indicator.indicator']['name'] = 'Sandbox';
$config['environment_indicator.indicator']['bg_color'] = '#48484a';
$config['environment_indicator.indicator']['fg_color'] = '#ffffff';

$settings['trusted_host_patterns'] = [
  '%DOMAIN%',
  '\.test$',
];

$settings['xmlsitemap_base_url'] = 'https://%DOMAIN%';

/**
 * Everything after this point should not be used in production, and rarely on
 * test.
 */
$settings['container_yamls'][] = DRUPAL_ROOT . '/sites/development.services.yml';
$settings['container_yamls'][] = DRUPAL_ROOT . '/sites/default/sandbox.services.yml';
$settings['cache']['bins']['render'] = 'cache.backend.null';
$settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';
$settings['cache']['bins']['page'] = 'cache.backend.null';
$config['system.performance']['css']['preprocess'] = FALSE;
$config['system.performance']['js']['preprocess'] = FALSE;

/**
 * Force disable adv_varnish to avoid HTTP errors when doing a cache-rebuild soon after syncing DB from live
 */
$config['adv_varnish.cache_settings']['available']['enable_cache'] = FALSE;
$config['adv_varnish.cache_settings']['general']['varnish_purger'] = FALSE; 

/**
 * Force enable reroute_email to avoid accidental emails to client/users
 */
$config['reroute_email.settings']['enable'] = TRUE;

/**
 * Assertions.
 *
 * The Drupal project primarily uses runtime assertions to enforce the
 * expectations of the API by failing when incorrect calls are made by code
 * under development.
 *
 * @see http://php.net/assert
 * @see https://www.drupal.org/node/2492225
 *
 * If you are using PHP 7.0 it is strongly recommended that you set
 * zend.assertions=1 in the PHP.ini file (It cannot be changed from .htaccess
 * or runtime) on development machines and to 0 in production.
 *
 * @see https://wiki.php.net/rfc/expectations
 */
assert_options(ASSERT_ACTIVE, TRUE);
\Drupal\Component\Assertion\Handle::register();

/**
 * Show all error messages, with backtrace information.
 *
 * In case the error level could not be fetched from the database, as for
 * example the database connection failed, we rely only on this value.
 */
$config['system.logging']['error_level'] = 'verbose';

/**
 * Disable caching for migrations.
 *
 * Uncomment the code below to only store migrations in memory and not in the
 * database. This makes it easier to develop custom migrations.
 */
# $settings['cache']['bins']['discovery_migration'] = 'cache.backend.memory';

/**
 * Allow test modules and themes to be installed.
 *
 * Drupal ignores test modules and themes by default for performance reasons.
 * During development it can be useful to install test extensions for debugging
 * purposes.
 */
//$settings['extension_discovery_scan_tests'] = TRUE;

/**
 * Enable access to rebuild.php.
 *
 * This setting can be enabled to allow Drupal's php and database cached
 * storage to be cleared via the rebuild.php page. Access to this page can also
 * be gained by generating a query string from rebuild_token_calculator.sh and
 * using these parameters in a request to rebuild.php.
 */
$settings['rebuild_access'] = FALSE;

/**
 * Skip file system permissions hardening.
 *
 * The system module will periodically check the permissions of your site's
 * site directory to ensure that it is not writable by the website user. For
 * sites that are managed with a version control system, this can cause problems
 * when files in that directory such as settings.php are updated, because the
 * user pulling in the changes won't have permissions to modify files in the
 * directory.
 */
$settings['skip_permissions_hardening'] = TRUE;
