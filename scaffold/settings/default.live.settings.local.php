<?php

$databases['default']['default'] = [
  'database' => '%DB_NAME%',
  'username' => '',
  'password' => '',
  'prefix' => '',
  'host' => 'localhost',
  'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
];

/**
 * Where do private files live?
 */
$settings['file_private_path'] = realpath($app_root . '/../private_files');

$config['environment_indicator.indicator']['name'] = 'Live';
$config['environment_indicator.indicator']['bg_color'] = '#808285';
$config['environment_indicator.indicator']['fg_color'] = '#ffffff';

/**
 * No automatic cron on live, we'll setup a linux cronjob for this specific site.
 */
$config['automated_cron.settings']['interval'] = 0;

$settings['trusted_host_patterns'] = [
  '%DOMAIN%',
  '\.cyberwoven.net$',
];

$settings['xmlsitemap_base_url'] = 'https://%DOMAIN%';
