<?php

$db_name = '%DB_NAME%';

/**
 * Test branch detection (can't run git as the www-data user)
 */
$raw_branch_name = basename(dirname(DRUPAL_ROOT));
if ($raw_branch_name != 'master' && $raw_branch_name != 'main') {
  $db_name .= '__' . $raw_branch_name;
}

// this config value should be set in both cli/php.ini and fpm/php.ini
$db_overrides_file = get_cfg_var('cyberwoven.db_overrides_file');
$db_overrides = [];
if (file_exists($db_overrides_file)) {
    $db_overrides = include $db_overrides_file;
}

$databases['default']['default'] = [
  'database'  => $db_name,
  'username'  => 'cyberwoven',
  'password'  => '',
  'prefix'    => '',
  'host'      => 'forest-db',
  'port'      => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver'    => 'mysql',
  ...$db_overrides,
];

/**
 * Where do private files live?
 */
$settings['file_private_path'] = realpath($app_root . '/../private_files');

$config['environment_indicator.indicator']['name'] = 'Test';
$config['environment_indicator.indicator']['bg_color'] = '#f05023';
$config['environment_indicator.indicator']['fg_color'] = '#ffffff';

/**
 * Automatic cron, even if it's disabled in the DB.
 */
$config['automated_cron.settings']['interval'] = 86400;

$settings['trusted_host_patterns'] = [
  '%DOMAIN%',
  '\.cyberwoven.net$',
];

$settings['xmlsitemap_base_url'] = 'https://%DOMAIN%';

/**
 * Everything after this point should not be used in production, and rarely on
 * test.
 */
// $settings['container_yamls'][] = DRUPAL_ROOT . '/sites/development.services.yml';
// $settings['container_yamls'][] = DRUPAL_ROOT . '/sites/default/sandbox.services.yml';

// $settings['cache']['bins']['render'] = 'cache.backend.null';
// $settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';
// $settings['cache']['bins']['page'] = 'cache.backend.null';
// $config['system.performance']['css']['preprocess'] = FALSE;
// $config['system.performance']['js']['preprocess'] = FALSE;
