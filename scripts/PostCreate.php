<?php

use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class PostCreate {

    public static function writeTemplatedFile($src, $dst, $vars) {
        $contents = file_get_contents($src);
        $contents = str_replace(array_keys($vars), array_values($vars), $contents);
        file_put_contents($dst, $contents);
    }

    public static function log($filename, $data) {
        $log_dir = dirname(__DIR__) . '/scratch';
        if (!is_dir($log_dir)) {
            mkdir($log_dir);
        }
        file_put_contents($log_dir . '/' . $filename, $data, FILE_APPEND);
    }

    public static function ScaffoldFiles($event) {
        if (isset($_SERVER['DRUPAL_DATABASE_NAME'])) {
            $db_name = $_SERVER['DRUPAL_DATABASE_NAME'];
        } else {
            do {
                $db_name = trim(readline("Database name? [0-9a-z_]+ "));
                $valid_db_name = preg_match('/^[0-9a-z_]+$/', $db_name);
            } while (!$valid_db_name);
        }

        if (isset($_SERVER['DRUPAL_THEME_NAME'])) {
            $theme_name = $_SERVER['DRUPAL_THEME_NAME'];
        } else {
            $theme_name = $db_name;
        }

        $project_dir = dirname(__DIR__);
        $domain = basename($project_dir);

        $vars = [
            '%DOMAIN%'  => $domain, 
            '%DB_NAME%' => $db_name,
            '%THEME%'   => $theme_name,
        ];

        $src_dir = $project_dir . '/scaffold/settings';
        $dst_dir = $project_dir . '/pub/sites/default';
        
        $sandbox_file = 'default.sandbox.settings.local.php';
        $test_file    = 'default.test.settings.local.php';
        $live_file    = 'default.live.settings.local.php';

        $src = $src_dir . '/' . $sandbox_file;
        $dst = $dst_dir . '/' . $sandbox_file;
        self::writeTemplatedFile($src, $dst, $vars);
        copy($dst, $dst_dir . '/settings.local.php');

        $src = $src_dir . '/' . $test_file;
        $dst = $dst_dir . '/' . $test_file;
        self::writeTemplatedFile($src, $dst, $vars);

        $src = $src_dir . '/' . $live_file;
        $dst = $dst_dir . '/' . $live_file;
        self::writeTemplatedFile($src, $dst, $vars);

        $install_cmd_args = [
            'drush', 
            'site:install', 
            'standard', 
            '--yes',
            '--account-name=cwadmin',
            '--account-mail=admin@example.com',
            sprintf('--db-url=mysql://%s@localhost/%s', $_SERVER['USER'], $db_name),
            sprintf('--site-name=%s', $domain),
        ];
        $install_cmd = new Process($install_cmd_args);
        $install_cmd->run();

        // executes after the command finishes
        if (!$install_cmd->isSuccessful()) {
            throw new ProcessFailedException($install_cmd);
        }

        echo $install_cmd->getOutput();
        self::log('drush-install.log', $install_cmd->getOutput());

        // MAKE SETTINGS WRITABLE AGAIN
        exec("chmod -R u+w $dst_dir");

        mkdir($project_dir . '/private_files');

        /**
         * The drush site:install hardcodes the current user's name in the 
         * $databases array. We want to replace that with $_SERVER['USER'].
         */
        $settings_lines = file($dst_dir . '/settings.php');
        $settings_content = '';
        foreach($settings_lines as &$line) {
            if (substr($line, 0, 16) == "  'username' => ") {
                $line = "  'username' => \$_SERVER['USER'],\n";
            } elseif (substr($line, 1, 57) == "settings['config_sync_directory'] = 'sites/default/files/") {
                $line = "\$settings['config_sync_directory'] = realpath(\$app_root . '/../config/sync');\n";
            }
            $settings_content .= $line;
        }

        /**
         * Append the settings.local conditional include
         */
        $settings_content .= <<<'EOT'
        if (file_exists($app_root . '/' . $site_path . '/settings.local.php')) {
          include $app_root . '/' . $site_path . '/settings.local.php';
        }
        EOT;
        file_put_contents($dst_dir . '/settings.php', $settings_content);

        /**
         * Enable these modules
         */
        $modules = [
            'address',
            'admin_toolbar',
            'admin_toolbar_tools',
            'adv_varnish',
            'ckeditor_accordion',
            'claro_tweaks',
            'config',
            'content_moderation',
            'contextual',
            'crop',
            'ctools',
            'cyberwoven_suite',
            'datetime',
            'editor',
            'editor_advanced_link',
            'entity_reference_revisions',
            'environment_indicator',
            'field_group',
            'focal_point',
            'help',
            'history',
            'honeypot',
            'image_widget_crop',
            'imce',
            'jquery_ui_accordion',
            'link',
            'linkit',
            'masquerade',
            'menu_block',
            'menu_link_content',
            'metatag',
            'metatag_open_graph',
            'metatag_verification',
            'module_filter',
            'options',
            'paragraphs',
            'path',
            'pathauto',
            'rabbit_hole',
            'redirect',
            'reroute_email',
            'robotstxt',
            'smart_trim',
            'smtp',
            'svg_image',
            'twig_field_value',
            'twig_tools',
            'twig_tweak',
            'unpublished_404',
            'webform',
            'webform_ui',
            'workflows',
            'xmlsitemap',
        ];

        $enable_cmd_args = [
            'drush', 
            'en', 
            '--yes',
            ...$modules,
        ];
        $enable_cmd = new Process($enable_cmd_args);
        $enable_cmd->run();

        // executes after the command finishes
        if (!$enable_cmd->isSuccessful()) {
            echo $enable_cmd->getErrorOutput();
            self::log('drush-enable.log', $enable_cmd->getErrorOutput());
        }

        echo $enable_cmd->getOutput();
        self::log('drush-enable.log', $enable_cmd->getOutput());

        /**
         * Copy theme files into place
         */
        $src_theme = $project_dir . '/scaffold/theme';
        $dst_theme = $project_dir . '/pub/themes/' . $theme_name;
        rename($src_theme, $dst_theme);

        self::writeTemplatedFile("$dst_theme/gulpfile.babel.js", "$dst_theme/gulpfile.babel.js", $vars);
        self::writeTemplatedFile("$dst_theme/THEME.info.yml", "$dst_theme/$theme_name.info.yml", $vars);
        self::writeTemplatedFile("$dst_theme/THEME.libraries.yml", "$dst_theme/$theme_name.libraries.yml", $vars);
        self::writeTemplatedFile("$dst_theme/THEME.theme", "$dst_theme/$theme_name.theme", $vars);

        unlink("$dst_theme/THEME.info.yml");
        unlink("$dst_theme/THEME.libraries.yml");
        unlink("$dst_theme/THEME.theme");

        /**
         * Enable the custom theme
         */
        $enable_theme_cmd = <<<EOT
            drush cr
            drush theme:enable $theme_name
            drush config-set system.theme default $theme_name --yes
        EOT;
        unset($output);
        exec($enable_theme_cmd, $output, $retval);
        $output_str = implode("\n", $output);
        file_put_contents($project_dir . '/scratch/post-create.log', $output_str, FILE_APPEND);
        
    }

    public static function ComposerCleanup($event) {
        $project_root = dirname(__DIR__);
        $composer_file_path = $project_root . '/composer.json';
        $composer_json_raw = file_get_contents($composer_file_path);
        $json = json_decode($composer_json_raw);

        /**
         * delete composer.json properties that the site doesn't need
         */
        unset($json->description);
        unset($json->license);
        unset($json->autoload);
        unset($json->scripts->{'post-create-project-cmd'});
        
        $name_parts = explode('/', $json->name);
        $json->name = $name_parts[0] . '/' . basename($project_root);
        
        $new_composer_json_raw = json_encode($json, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
        file_put_contents($composer_file_path, $new_composer_json_raw);

    }
}
