#!/bin/bash

echo "Deleting installation files..."
rm -rf scripts
rm -rf scaffold
rm readme.md
rm default.packages.json


echo "Creating local git repo..."
git init . -b master
git add .
git commit -m "post install"
