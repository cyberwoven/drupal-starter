<?php

use Drupal\shortcut\Entity\Shortcut;
use Drupal\field\Entity\FieldStorageConfig;

// We install some menu links, so we have to rebuild the router, to ensure the
// menu links are valid. (Taken from standard.install.)
\Drupal::service('router.builder')->rebuildIfNeeded();

// Populate the default shortcut set.
$shortcut = Shortcut::create([
    'shortcut_set' => 'default',
    'title' => t('Add content'),
    'weight' => -20,
    'link' => ['uri' => 'internal:/node/add'],
]);
$shortcut->save();

$shortcut = Shortcut::create([
    'shortcut_set' => 'default',
    'title' => t('All content'),
    'weight' => -19,
    'link' => ['uri' => 'internal:/admin/content'],
]);
$shortcut->save();

/**
 * Get ride of the Comment field, so we can uninstall the comment module
 */
$field = FieldStorageConfig::loadByName("node", "comment"); 
if ($field) {
    $field->delete();
}

/**
 * Uninstall modules we don't use
 */
$modules = [
    'big_pipe',
    'comment',
    'contact',
    'search',
];

\Drupal::service('module_installer')->uninstall($modules);
