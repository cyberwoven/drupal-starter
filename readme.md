# Cyberwoven Drupal Project Starter

Normal usage:
```
composer create-project cyberwoven/drupal-starter www.example.com
```

To create a project from the local repo, assuming that:
 1. bitbucket.org/cyberwoven/drupal-starter has been cloned locally into ~/Sites/drupal-starter
 2. ~/Sites/drupal-starter/default.packages.json has bene copied to ~/Sites/drupal-starter/packages.json
 3. the contents of packages.json has been modified so that the "url" value points to the local path

```shell
composer create-project --repository-url=/Users/USERNAME/Sites/drupal/packages.json cyberwoven/drupal-starter www.example.com
```